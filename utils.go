package vcstools

import (
    "bytes"
    "os"
    "os/exec"
)

// Check if file or directory exists
func isPathExists(path string) bool {
    _, err := os.Stat(path)
    if err == nil || !os.IsNotExist(err) {
        return true
    }
    return false
}

// Run command and read it's output
func runCommand(dir, cmd string, args ...string) (out []byte, err error) {
    c := exec.Command(cmd, args...)
    c.Dir = dir

    var buff bytes.Buffer
    c.Stdout, c.Stderr = &buff, &buff
    err = c.Run()
    out = buff.Bytes()
    return
}
