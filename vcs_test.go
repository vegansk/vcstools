package vcstools

import (
    "io/ioutil"
    "os"
    "os/exec"
    "path/filepath"
    "testing"
)

func showErrorAndExit(t *testing.T, err interface{}) {
    if err == nil {
        return
    }
    t.Fatal(err)
}

func mustCreateTempDir(t *testing.T) string {
    dir, err := ioutil.TempDir("", "vcs")
    if err != nil {
        showErrorAndExit(t, err)
    }
    return dir
}

func mustCreateFile(t *testing.T, dir, name string) {
    fi, err := os.Create(filepath.Join(dir, name))
    if err != nil {
        showErrorAndExit(t, err)
    }
    fi.Close()
}

func mustCreateTempFile(t *testing.T, dir string) string {
    file, err := ioutil.TempFile(dir, "vcs")
    if err != nil {
        showErrorAndExit(t, err)
    }
    file.Close()
    return file.Name()
}

func mustRemoveDir(dir string) {
    os.RemoveAll(dir)
}

func isToolPresent(name string) bool {
    _, err := exec.LookPath(name)
    return err == nil
}

func mustRunCommand(t *testing.T, dir, cmd string, args ...string) {
    _, err := runCommand(dir, cmd, args...)
    if err != nil {
        showErrorAndExit(t, err)
    }
}

func prepareGitRepo(t *testing.T, root string, tags []string) {
    mustRunCommand(t, root, "git", "init")
    mustRunCommand(t, root, "git", "add", "-A")
    mustRunCommand(t, root, "git", "commit", "-m", "1234")
    for _, tag := range tags {
        mustRunCommand(t, root, "git", "tag", tag)
    }
}

func contains(what string, vals []string) bool {
    for _, v := range vals {
        if what == v {
            return true
        }
    }
    return false
}

func TestGit(t *testing.T) {
    if !isToolPresent("git") {
        t.Skip("Git not found")
    }

    v, err := getGitVersion()
    showErrorAndExit(t, err)
    t.Logf("Git version = %v", v)
    cmp, err := compareGitVersion(1, 7, 9, 5)
    showErrorAndExit(t, err)
    t.Logf("Compare git version with 1.7.9.5 = %v", cmp)

    root := mustCreateTempDir(t)
    defer mustRemoveDir(root)
    mustCreateTempFile(t, root)

    tags := []string{"0.1", "0.2", "0.3"}
    prepareGitRepo(t, root, tags)

    vcs, err := Open(root)
    showErrorAndExit(t, err)

    vcsTags, err := vcs.GetTags()
    if err != nil {
        showErrorAndExit(t, err)
    }
    for _, tag := range tags {
        if !contains(tag, vcsTags) {
            t.Errorf("Tag %s not found!", tag)
        }
        showErrorAndExit(t, vcs.CheckoutTag(tag))
    }
}

func prepareHgRepo(t *testing.T, root string, tags []string) {
    mustRunCommand(t, root, "hg", "init")
    mustRunCommand(t, root, "hg", "add")
    mustRunCommand(t, root, "hg", "commit", "-u", "test", "-m", "test")
    for _, tag := range tags {
        mustRunCommand(t, root, "hg", "tag", "-u", "test", tag)
    }
}

func TestHg(t *testing.T) {
    if !isToolPresent("hg") {
        t.Skip("Hg not found")
    }

    root := mustCreateTempDir(t)
    defer mustRemoveDir(root)
    mustCreateTempFile(t, root)
    mustCreateFile(t, root, ".hgtags")

    tags := []string{"0.1", "0.2", "0.3"}
    prepareHgRepo(t, root, tags)

    vcs, err := Open(root)
    showErrorAndExit(t, err)

    vcsTags, err := vcs.GetTags()
    if err != nil {
        showErrorAndExit(t, err)
    }
    for _, tag := range tags {
        if !contains(tag, vcsTags) {
            t.Errorf("Tag %s not found!", tag)
        }
        showErrorAndExit(t, vcs.CheckoutTag(tag))
    }
}

func prepareBzrRepo(t *testing.T, root string, tags []string) {
    mustRunCommand(t, root, "bzr", "init")
    mustRunCommand(t, root, "bzr", "add")
    mustRunCommand(t, root, "bzr", "commit", "-m", "test")
    for _, tag := range tags {
        mustRunCommand(t, root, "bzr", "tag", tag)
    }
}

func TestBzr(t *testing.T) {
    if !isToolPresent("bzr") {
        t.Skip("Bzr not found")
    }

    root := mustCreateTempDir(t)
    defer mustRemoveDir(root)
    mustCreateTempFile(t, root)

    tags := []string{"0.1", "0.2", "0.3"}
    prepareBzrRepo(t, root, tags)

    vcs, err := Open(root)
    showErrorAndExit(t, err)

    vcsTags, err := vcs.GetTags()
    if err != nil {
        showErrorAndExit(t, err)
    }
    for _, tag := range tags {
        if !contains(tag, vcsTags) {
            t.Errorf("Tag %s not found!", tag)
        }
        showErrorAndExit(t, vcs.CheckoutTag(tag))
    }
}
