package vcstools

import (
    "fmt"
    "os"
    "path/filepath"
    "regexp"
    "strconv"
    "strings"
)

// Standard exceptions
var (
    errNotImplemented   = fmt.Errorf("Method not implemented")
    ErrNoLocalRepoFound = fmt.Errorf("Not repository found in given directory")
    errNotDir           = fmt.Errorf("This is not a directory")
)

// Version control system interface
type VCS interface {
    // Check if directory belongs to this VCS
    checkDir(dir string) (bool, error)
    // Open directory
    openDir(dir string) error
    // Update repo
    Update() error
    // Get VCS tags
    GetTags() ([]string, error)
    // Checkout tag
    CheckoutTag(tag string) error
}

type vcsFactory func() VCS

// Available VCS factories
var vcsFactories = []vcsFactory{
    newGitVCS,
    newHgVCS,
    newBzrVCS,
}

// Open existing directory with VCS
func Open(dir string) (vcs VCS, err error) {
    var res bool
    for _, f := range vcsFactories {
        vcs = f()
        if res, err = vcs.checkDir(dir); err != nil {
            return nil, err
        }
        if res {
            if err = vcs.openDir(dir); err != nil {
                return nil, err
            }
            return
        }
    }
    return nil, ErrNoLocalRepoFound
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// VCS base

// Base type for VCS
type vcsBase struct {
    Dir string // Path to working copy
    Cmd string // VCS command
}

func (v *vcsBase) checkDir(dir string) (bool, error) {
    return checkVCSBySubDir(dir, "."+v.Cmd)
}

func (v *vcsBase) openDir(dir string) error {
    v.Dir = dir
    return nil
}

func (v *vcsBase) run(args ...string) ([]string, error) {
    outBytes, err := runCommand(v.Dir, v.Cmd, args...)
    if err != nil {
        fmt.Println(string(outBytes))
        return nil, err
    }
    return strings.Split(string(outBytes), "\n"), nil
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Git VCS

type gitVCS struct {
    vcsBase
}

var gitReTags = regexp.MustCompile(`(?:tags|origin)/(\S+)$`)

func newGitVCS() VCS {
    return &gitVCS{
        vcsBase{
            Cmd: "git",
        },
    }
}

func (v *gitVCS) GetTags() (tags []string, err error) {
    out, err := v.run("show-ref")
    if err != nil {
        return
    }
    for _, s := range out {
        if f := gitReTags.FindStringSubmatch(s); len(f) == 2 {
            tags = append(tags, f[1])
        }
    }
    return
}

func (v *gitVCS) CheckoutTag(tag string) (err error) {
    if tag == "*" || tag == "" {
        _, err = v.run("checkout", "master")
    } else {
        _, err = v.run("checkout", tag)
    }
    return
}

func (v *gitVCS) Update() (err error) {
    ver, err := compareGitVersion(1, 7, 9, 5)
    if err != nil {
        return err
    }
    _, err = v.run("fetch", "--all")
    if err != nil {
        return err
    }
    if ver >= 0 {
        _, err = v.run("fetch", "--prune", "--tags")
    } else {
        _, err = v.run("fetch", "--tags")
    }
    if err != nil {
        return err
    }
    _, err = v.run("pull", "--ff-only")
    if err != nil {
        return err
    }
    return
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Hg VCS

type hgVCS struct {
    vcsBase
}

var hgReTags = regexp.MustCompile(`^(\S+)`)

func newHgVCS() VCS {
    return &hgVCS{
        vcsBase{
            Cmd: "hg",
        },
    }
}

func (v *hgVCS) GetTags() (tags []string, err error) {
    out, err := v.run("tags")
    if err != nil {
        return
    }
    for _, s := range out {
        if f := hgReTags.FindStringSubmatch(s); len(f) == 2 {
            tags = append(tags, f[1])
        }
    }
    return
}

func (v *hgVCS) CheckoutTag(tag string) (err error) {
    if tag == "*" || tag == "" {
        _, err = v.run("update", "default")
    } else {
        _, err = v.run("update", "-r", tag)
    }
    return
}

func (v *hgVCS) Update() (err error) {
    _, err = v.run("pull")
    return
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Bazaar VCS

type bzrVCS struct {
    vcsBase
}

var bzrReTags = regexp.MustCompile(`^(\S+)`)

func newBzrVCS() VCS {
    return &bzrVCS{
        vcsBase{
            Cmd: "bzr",
        },
    }
}

func (v *bzrVCS) GetTags() (tags []string, err error) {
    out, err := v.run("tags")
    if err != nil {
        return
    }
    for _, s := range out {
        if f := bzrReTags.FindStringSubmatch(s); len(f) == 2 {
            tags = append(tags, f[1])
        }
    }
    return
}

func (v *bzrVCS) CheckoutTag(tag string) (err error) {
    if tag == "*" || tag == "" {
        _, err = v.run("update", "-r", "revno:-1")
    } else {
        _, err = v.run("update", "-r", tag)
    }
    return
}

func (v *bzrVCS) Update() (err error) {
    _, err = v.run("pull", "--overwrite")
    return
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Misc functions

// Check VCS by subdirectory existence
func checkVCSBySubDir(dir, subdir string) (bool, error) {
    if !isPathExists(dir) {
        return false, ErrNoLocalRepoFound
    }
    if !isPathExists(filepath.Join(dir, subdir)) {
        return false, nil
    }
    return true, nil
}

func getGitVersion() (ver []int, err error) {
    out, err := runCommand(os.TempDir(), "git", "--version")
    if err != nil {
        return
    }
    verStr := string(out)
    for _, v := range regexp.MustCompile(`\d+`).FindAllStringSubmatch(verStr, -1) {
        v, e := strconv.ParseInt(v[0], 10, 32)
        if e != nil {
            continue
        }
        ver = append(ver, int(v))
    }
    return
}

// Return -1 if git version lesser than given, 0 if equal, 1 if greater
func compareGitVersion(ver ...int) (res int, err error) {
    gitVer, err := getGitVersion()
    if err != nil {
        return
    }
    l, l1 := len(ver), len(gitVer)
    if l1 < l {
        l = l1
    }

    for idx := 0; idx < l; idx++ {
        switch {
        case ver[idx] > gitVer[idx]:
            return -1, nil
        case ver[idx] < gitVer[idx]:
            return 1, nil
        }
    }
    return 0, nil
}
